# Generated by Django 4.2.11 on 2024-03-27 01:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("register", "0015_accomm_option"),
    ]

    operations = [
        migrations.AddField(
            model_name="attendee",
            name="affiliation",
            field=models.TextField(blank=True),
        ),
    ]
