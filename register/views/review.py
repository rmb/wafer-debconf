from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.forms import Form

from bursary.models import Bursary
from register.models import Attendee, Food
from register.models.accommodation import accomm_option_choices
from register.views.core import RegisterStep


class ReviewView(RegisterStep):
    title = 'Review'
    form_class = Form
    template_name = 'register/page/review.html'

    def get_context_data(self, **kwargs):
        user = self.request.user

        try:
            attendee = user.attendee
        except Attendee.DoesNotExist:
            attendee = None

        try:
            bursary = user.bursary
        except Bursary.DoesNotExist:
            bursary = None

        try:
            food = user.attendee.food
        except ObjectDoesNotExist:
            food = None

        try:
            accomm = user.attendee.accomm
        except ObjectDoesNotExist:
            accomm = None

        try:
            child_care = user.attendee.child_care
        except ObjectDoesNotExist:
            child_care = None

        try:
            visa = user.attendee.visa
        except ObjectDoesNotExist:
            visa = None

        context = super().get_context_data(**kwargs)

        context.update({
            'DEBCONF_COLLECT_AFFILIATION': settings.DEBCONF_COLLECT_AFFILIATION,
            'DEBCONF_SHOE_SIZES': settings.DEBCONF_SHOE_SIZES,
            'DEBCONF_BURSARY_CURRENCY': settings.DEBCONF_BURSARY_CURRENCY,
            'DEBCONF_BURSARY_CURRENCY_SYMBOL':
                settings.DEBCONF_BURSARY_CURRENCY_SYMBOL,
            'RECONFIRMATION': settings.RECONFIRMATION,
            'accomm': accomm,
            'attendee': attendee,
            'bursary': bursary,
            'child_care': child_care,
            'food': food,
            'profile': user.userprofile,
            'user': user,
            'visa': visa,
        })

        if accomm and accomm.option:
            context.update({
                'accomm_option': dict(accomm_option_choices())[accomm.option],
            })
        if attendee:
            context.update({
                'fee': Attendee.FEES[attendee.fee],
                'gender': Attendee.GENDERS[attendee.gender],
                't_shirt_size':
                    dict(settings.DEBCONF_T_SHIRT_SIZES)[attendee.t_shirt_size],
            })
        if bursary:
            context['bursary_need'] = Bursary.BURSARY_NEEDS.get(bursary.need)
        if food:
            context['diet'] = Food.DIETS[food.diet]

        return context
